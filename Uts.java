/* Nama : Imam Rizki Saputra
 * Nim  : 301230013
 * Kelas: IF - 1B
 */

import java.util.Scanner;
 
 public class uts {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] namaProduk = {"Specs", "Mills", "Ortuseight", "Calci", "Puma"};
        int[] hargaProduk = {400000, 500000, 350000, 250000, 450000};
        int[] stokProduk = {20, 10, 15, 22, 8};
       
        int totalHarga = 0;
        boolean isRunning = true;
        
        System.out.println("|===========================================|" );
        System.out.println("|             Toko Sport Imam               |" );
        System.out.println("|===========================================|" );
        System.out.println("| No |\t nama produk |\t  Harga  |\t stok   |" );
        System.out.println("| 1. |\t specs       |\t  400000 |\t 20     |" );
        System.out.println("| 2. |\t Mills       |\t  500000 |\t 10     |" );
        System.out.println("| 3. |\t Ortuseight  |\t  350000 |\t 15     |" );
        System.out.println("| 4. |\t Calci       |\t  250000 |\t 22     |" );
        System.out.println("| 5. |\t Puma        |\t  450000 |\t 8      |" );
        System.out.println("|===========================================|" );
        System.out.println("| 0. |\t Selesai                            |" );
        System.out.println("|===========================================|" );
        while (isRunning) {
            
            for (int i = 0; i < namaProduk.length; i++) 
            {}
            
            System.out.print(" Pilih produk : ");
            int pilihan = input.nextInt();
            
            if (pilihan >= 1 && pilihan <= namaProduk.length) {
                int indexProduk = pilihan - 1;
                
                if (stokProduk[indexProduk] > 0) {
                    totalHarga += hargaProduk[indexProduk];
                    stokProduk[indexProduk]--;

                    System.out.println("Produk " + namaProduk[indexProduk] + " telah ditambahkan ke keranjang.");
                } else {
                    System.out.println("Maaf, stok " + namaProduk[indexProduk] + " kosong.");
                }
            } else if (pilihan == 0) {
                isRunning = false;
            } else {
                System.out.println("Pilihan tidak valid.");
            }
        }
    
    System.out.println("|===================================================|");
        System.out.println("Total Harga: " + totalHarga );
        
        input.close();

    } 

}



        
